import { Outlet } from 'react-router-dom';
import ResponsiveNavbar from './components/ResponsiveNavbar';
import { useState } from 'react';

function App() {
  //searchText of ResponsiveNavbar
  const [searchText, setSearchText] = useState('');

  const handleOnChange = ({ target }) => {
    setSearchText(target.value);
  }

  return (
    <div className="App">
      <ResponsiveNavbar handleOnChange={handleOnChange}/>
      <Outlet context={searchText}/>
    </div>
  );
}

export default App;
