import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import Home from './Home';

it('should render todos', async () => {
  render(<Home />);

  expect(await screen.findByText('Cleaning the kitchen!')).toBeInTheDocument();
  expect(await screen.findByText('Book flight to NY')).toBeInTheDocument();
  expect(await screen.findByText('Cut Hair')).toBeInTheDocument();
  expect(await screen.findByText('Read Article in David Walsh blog')).toBeInTheDocument();
})

it('should filter by category', async () => {
  render(<Home />);

  //find every todo
  const todo1 = await screen.findByText('Cleaning the kitchen!');
  const todo2 = await screen.findByText('Book flight to NY');
  const todo3 = await screen.findByText('Cut Hair');
  const todo4 = await screen.findByText('Read Article in David Walsh blog');

  //select category=private -> filter todos by category=private 
  userEvent.selectOptions(screen.getByRole('combobox', { label: 'select-category'}), 'private');

  //check if only todos with category=private are in the document
  expect(todo1).toBeInTheDocument();
  expect(todo2).not.toBeInTheDocument();
  expect(todo3).not.toBeInTheDocument();
  expect(todo4).not.toBeInTheDocument();
})

it('should filter by isCompleted', async () => {
  render(<Home />);

    //find every todo
    const todo1 = await screen.findByText('Cleaning the kitchen!');
    const todo2 = await screen.findByText('Book flight to NY');
    const todo3 = await screen.findByText('Cut Hair');
    const todo4 = await screen.findByText('Read Article in David Walsh blog');

    //set isCompleted filter to true
    userEvent.click(screen.getByRole('checkbox', { name: 'only completed'}));

    //check if only todos with isCompleted=true are in the document
    expect(todo1).toBeInTheDocument();
    expect(todo2).not.toBeInTheDocument();
    expect(todo3).not.toBeInTheDocument();
    expect(todo4).not.toBeInTheDocument();
})

it('should filter by category and isCompleted', async () => {
  render(<Home />);

  //find every todo
  const todo1 = await screen.findByText('Cleaning the kitchen!');
  const todo2 = await screen.findByText('Book flight to NY');
  const todo3 = await screen.findByText('Cut Hair');
  const todo4 = await screen.findByText('Read Article in David Walsh blog');

  //select category=private -> filter todos by category=private 
  userEvent.selectOptions(screen.getByRole('combobox', { label: 'select-category'}), 'private');

  //set isCompleted filter to true
  userEvent.click(screen.getByRole('checkbox', { name: 'only completed'}));

  //check if only todos with isCompleted=true & category=private are in the document
  expect(todo1).toBeInTheDocument();
  expect(todo2).not.toBeInTheDocument();
  expect(todo3).not.toBeInTheDocument();
  expect(todo4).not.toBeInTheDocument();
})