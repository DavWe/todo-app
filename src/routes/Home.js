//Bootstrap
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

//components
import TodoList from '../components/TodoList';
import Settings from '../components/Settings';
import CreateModal from '../components/CreateModal';

//React
import { useEffect, useState } from 'react';
import { useOutletContext } from "react-router-dom";

//service
import axios from '../service/axios.service';

const Home = () => {
    //Array of todos
    const [todosArr, setTodosArr] = useState([])

    //Filter
    const [filteredArr, setFilteredArr] = useState([]);
    const [showCompleted, setShowCompleted] = useState(false);
    const [showCategory, setShowCategory] = useState('all');
    //searchText of ResponsiveNavbar
    const searchText = useOutletContext();

    //CreateModal
    const [showModal, setShowModal] = useState(false);

    useEffect(() => {
      let arr = todosArr;
      if(!showCompleted && showCategory === 'all') {
        setFilteredArr(todosArr);
      }
      if(showCompleted) {
        arr = arr.filter(todo => todo.isCompleted === true);
      }
      if(showCategory !== 'all') {
        arr = arr.filter(todo => todo.category === showCategory);
      }
      if(searchText !== '' && searchText !== null) {
        arr = arr.filter(todo => todo.title.toLowerCase().includes(searchText.toLowerCase()));
      }
      setFilteredArr(arr);
    }, [showCompleted, showCategory, todosArr, searchText]);

    //fetches todos
    useEffect(() => {
      const fetchTodos = async () => {
        const todoResponse = await axios.get('/todos');
        if(todoResponse.status === 200) {
          setTodosArr(todoResponse.data);
          setFilteredArr(todoResponse.data);
        }
      }
      fetchTodos();
    }, [])

    //handlers
    const handleCompletedChange = ({ target }) => {
      setShowCompleted(target.checked);
    }

    const handleCategoryChange = ({ target }) => {
      setShowCategory(target.value);
    }

    const handleModalShow = () => {
      setShowModal(true);
    }

    const handleModalClose = () => {
      setShowModal(false);
    }

    const handleDeleteAll = async () => {
      const deleteAllResponse = await axios.delete('/todos');
      if(deleteAllResponse.status === 200) {
        setTodosArr([]);
      }
    }

    const handleDelete = async (id) => {
      const deleteResponse = await axios.delete('/todos/' + id);
      if(deleteResponse.status === 200) {
        setTodosArr(prev => prev.filter(todos => todos.id !== id));
      }
    }

    const handleAddTodo = (todo) => {
      setTodosArr(prev => [...prev, todo]);
    }

    const handleCheckClick = (id) => {
      setTodosArr(prev => {
        return prev.map(todo => {
          if(todo.id === id) {
            return {...todo, isCompleted: !todo.isCompleted}
          }
          return todo;
        })
      })
    }

  return (
    <div className="p-2">
      <CreateModal show={showModal} handleModalClose={handleModalClose} handleAddTodo={handleAddTodo} />
      <Row>
        <Col xs={12} md={3}>
          <Settings 
            handleCompletedChange={handleCompletedChange} 
            handleCategoryChange={handleCategoryChange} 
            handleDeleteAll={handleDeleteAll}
            handleModalShow={handleModalShow}
          />
        </Col>
        <Col xs={12} md={9}>
          <TodoList todosArr={filteredArr} handleDelete={handleDelete} handleCheckClick={handleCheckClick}/>
        </Col>
      </Row>
    </div>)
}

export default Home;