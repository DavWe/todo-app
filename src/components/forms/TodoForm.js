//service
import axios from '../../service/axios.service';

import * as Yup from 'yup';
import { TextInput, Select } from './formComponents';
import { Formik, Form } from 'formik';

const TodoForm = ({ handleAddTodo }) => {
  return (
    <Formik
    initialValues={{
      title: '',
      description: '',
      category: 'general'
    }}
    validationSchema={Yup.object({
      title: Yup.string()
        .max(15, 'Max 15 characters')
        .required('Required'),
      description: Yup.string()
        .max(100, 'Max 100 characters')
        .default(''),
      category: Yup.string().matches(/(general|private|work)/)
        .defined()
    })}
    onSubmit={async (values) => {
      //add isCompleted
      values.isCompleted = false;
      const response = await axios.post('/todos', values);
      if(response.status === 200) {
        handleAddTodo(response.data);
      }
    }}
    >
      <Form id="createTodo">
        <TextInput 
          label="Title"
          name="title"
          type="text"
        />

        <TextInput 
          label="Description"
          name="description"
          as="textarea"
        />

        <Select
          label="Category"
          name="category"
        >
          <option>general</option>
          <option>private</option>
          <option>work</option>
        </Select>
      </Form>
    </Formik>
  )
}

export default TodoForm;