import { useField } from 'formik';
import Form from 'react-bootstrap/Form';

//errorMessage style
const errorStyle = {
  color: 'red',
  fontSize: '0.8rem'
}

export const TextInput = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <>
      <Form.Label htmlFor={props.id || props.name}>{label}</Form.Label>
      <Form.Control {...field} {...props} />
      {meta.touched && meta.error ? (
        <p style={errorStyle}>{meta.error}</p>
      ) : null}
    </>
  );
};

export const Select = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <div>
      <Form.Label htmlFor={props.id || props.name}>{label}</Form.Label>
      <Form.Select {...field} {...props} />
      {meta.touched && meta.error ? (
        <p style={errorStyle}>{meta.error}</p>
      ) : null}
    </div>
  );
};