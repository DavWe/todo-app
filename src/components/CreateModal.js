//Bootstrap
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

//components
import TodoForm from './forms/TodoForm';

const CreateModal = ({ show, handleModalClose, handleAddTodo }) => {
  return (
    <Modal show={show} onHide={handleModalClose}>
      <Modal.Header closeButton>
        <Modal.Title>Create Todo</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <TodoForm handleAddTodo={handleAddTodo}/>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleModalClose}>
          close
        </Button>
        <Button variant="primary" type="submit" form="createTodo" onClick={handleModalClose}>
          submit
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default CreateModal;