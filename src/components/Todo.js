//Bootstrap
import Card from 'react-bootstrap/Card';
import Badge from 'react-bootstrap/Badge';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

const Todo = ({ id, title, description, category, isCompleted, handleDelete, handleCheckClick}) => {

  //determine the style of Badge component based on category
  const badgeStyle = () => {
    switch(category) {
      case 'private': return 'warning';
      case 'work': return 'danger';
      case 'general': return 'primary';
      default: return 'primary';
    }
  }

  return (
    <Card className="p-2 my-1" bg="success" border="dark">
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Card.Title>{title}</Card.Title>
        <Form.Check 
          type="checkbox"
          checked={isCompleted}
          onChange={() => handleCheckClick(id)}
        />
      </div>
      <Card.Text>{description}</Card.Text>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Button onClick={() => handleDelete(id)} variant="dark" size="sm" style={{ width: '4rem'}}>delete</Button>
        <Badge pill bg={badgeStyle()} style={{ width: 'fit-content', float: 'right' , alignSelf: 'flex-end'}}>{category}</Badge>
      </div>
    </Card>);
}

export default Todo;