import { Button, Card, Form } from "react-bootstrap"

const Settings = ({ handleCategoryChange, handleCompletedChange, handleDeleteAll, handleModalShow }) => {
  return (
    <Card bg="light" className="p-2">
      <Card.Title>Options</Card.Title>
      <div className="button-group">
        <Button onClick={handleModalShow} className="me-2" variant="success">create</Button>
        <Button onClick={handleDeleteAll} variant="danger">delete all</Button>
      </div>

      <Card.Title className="mt-3">Filter</Card.Title>
      <Form.Check id="only completed" onClick={handleCompletedChange} label="only completed"/>
      <Form.Select label="select-category" onChange={handleCategoryChange}>
          <option>all</option>
          <option>private</option>
          <option>work</option>
          <option>general</option>
        </Form.Select>
    </Card>
  )
}

export default Settings;