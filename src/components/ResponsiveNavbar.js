//Bootstrap
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';

//Assets
import logo from '../assets/galactus-logo-skaliert-big.png';

const ResponsiveNavbar = ({ handleOnChange }) => {
  return (
    <Navbar data-testid="navbar" collapseOnSelect expand="md" bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href="#home">
          <img
            src={logo}
            width="30"
            height="30"
            className="d-inline-block align-top"
            alt="React Bootstrap logo"
          />
        </Navbar.Brand>
        <Navbar.Brand href="#home">Todo-App</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="#home">Home</Nav.Link>
          </Nav>
          <Form className="d-flex">
            <FormControl
              type="search"
              placeholder="Search todos..."
              className="me-2"
              aria-label="Search"
              onChange={handleOnChange}
            />
          </Form>
          <Button variant="outline-light">search</Button>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default ResponsiveNavbar;