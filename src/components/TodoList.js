//Bootstrap
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

//components
import Todo from './Todo';

const TodoList = ({ todosArr, handleDelete, handleCheckClick }) => {

  const todos = todosArr.map(todo => (<Col key={todo.id} xs={12} md={6} xl={4}>
                                       <Todo id={todo.id} title={todo.title} description={todo.description} category={todo.category} isCompleted={todo.isCompleted} handleDelete={handleDelete} handleCheckClick={handleCheckClick} key={todo.id}/>
                                      </Col>))

  return (
    <div>
      <Row>
        {todos}
      </Row>
    </div>
  )
}

export default TodoList;