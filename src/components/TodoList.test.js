import { render, screen } from '@testing-library/react';
import TodoList from './TodoList';

//change mockData
it('renders Todo components', () => {
  const mockData = [
    { 
      id: "523324",
      title: "Cleaning the kitchen!",
      description: "To it very fast",
      category: "private",
      isCompleted: true
    },
    { 
      id: "523323",
      title: "Do homework",
      description: "p.233 ex. 5 + p.234 ex. 1",
      category: "private",
      isCompleted: false
    },
    { 
      id: "523320",
      title: "Cut Hair",
      description: "not more than 30$",
      category: "general",
      isCompleted: false
    }
  ];

  render(<TodoList todosArr={mockData}/>);

  const todoComponent1 = screen.getByText(/Cleaning the kitchen!/);
  const todoComponent2 = screen.getByText(/Do homework/);
  const todoComponent3 = screen.getByText(/Cut Hair/);

  expect(todoComponent1).toBeInTheDocument();
  expect(todoComponent2).toBeInTheDocument();
  expect(todoComponent3).toBeInTheDocument();
});