import { rest } from 'msw';

let todosArray = [
  { 
    id: "523320",
    title: "Cleaning the kitchen!",
    description: "To it very fast",
    category: "private",
    isCompleted: true
  },
  { 
    id: "523321",
    title: "Book flight to NY",
    description: "Seat near emergency exit",
    category: "work",
    isCompleted: false
  },
  { 
    id: "523322",
    title: "Cut Hair",
    description: "not more than 30$",
    category: "general",
    isCompleted: false
  },
  { 
    id: "523323",
    title: "Read Article in David Walsh blog",
    description: "use RSS feed app",
    category: "general",
    isCompleted: false
  }
];

export const handlers = [
  // Handles a POST /todos request
  rest.post('/todos', (req, res, ctx) => {
    const todo = req.body;
    todo.id = Math.floor(Math.random() * 100).toString();

    todosArray.push(todo);

    return res(
      ctx.status(200),
      ctx.json(todo)
    )
  }),

  // Handles a GET /todos request
  rest.get('/todos', (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json(todosArray)
    )
  }),

  // Handles a DELETE /todos request (deletes all todos)
  rest.delete('/todos', (req, res, ctx) => {
    todosArray = [];

    return res(
      ctx.status(200),
      ctx.json(todosArray)
    )
  }),

  // Handles a DELETE /todos/:id request (deletes specific todo with id)
  rest.delete('/todos/:id', (req, res, ctx) => {
    todosArray = todosArray.filter(todo => todo.id !== req.params.id);

    return res(
      ctx.status(200)
    )
  })
];