import { render, screen } from '@testing-library/react';
import App from './App';

it('renders navbar', () => {
  render(<App />);

  const navbar = screen.getByTestId('navbar');

  expect(navbar).toBeInTheDocument();
});
