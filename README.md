# Todo-App

This is a simple Todo-App built with React. You can create, delete and filter Todos.

## Third-party libraries

Styling: React-bootstrap [Github](https://github.com/react-bootstrap/react-bootstrap)\
REST-API: MSW [GitHub](https://github.com/mswjs/msw)\
Http-Request: Axios [Github](https://github.com/axios/axios)\
Forms: Formik [GitHub](https://github.com/jaredpalmer/formik)\
Testing: Jest [GitHub](https://github.com/facebook/jest), React-Testing-Library [Github](https://github.com/testing-library/react-testing-library)

## Start project

Run `npm install` in the project directory and then `npm start` to start the development server (Port 3000).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes. You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode. See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder. It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes. Your app is ready to be deployed!

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
